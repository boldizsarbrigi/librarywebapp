<%@page import="com.sda.librarywebapp.service.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Brigi's Book Library</title>
</head>
<body>
	<h1>Library Web App</h1>

	<%!int i;%>
	<table border="1">
		<c:forEach items="${BookService.listBooks()}" var="books">
			<tr>
				<td><c:out value="${books.getId()}"></c:out></td>
				<td><c:out value="${books.getBookTitle()}"></c:out></td>
				<td><c:out value="${books.getBookAuthor()}"></c:out></td>
				<td><c:out value="${books.getReleaseDate()}"></c:out></td>
				<td>
					<form action="http://localhost:8080/LibraryWebApp/deleteBook">
						<input type="hidden" name="idBook" value="${books.getId()}">
						<input type="Submit" value="Delete Book">
				</td>
				</form>
			</tr>
		</c:forEach>
		
	</table>
	<br>
	<form action="http://localhost:8080/LibraryWebApp/addBook" method="post">
		<input type="text" name="idBook" placeholder="BookId"> <input
			type="text" name="bookTitle" placeholder="BookTitle"> <input
			type="text" name="bookAuthor" placeholder="BookAuthor"> <input
			type="text" name="releaseDate" placeholder="ReleaseDate"> <input
			type="submit" value="Add Book">
	</form>
	

</body>
</html>