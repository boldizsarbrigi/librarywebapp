package com.sda.librarywebapp.model;

import java.util.Date;

public class Book {

	private int id;
	private String bookTitle;
	private String bookAuthor;
	private Date releaseDate;
		
	public Book() {
	}

	public Book(int id, String bookTitle, String bookAuthor, Date releaseDate) {
		this.id = id;
		this.bookTitle = bookTitle;
		this.bookAuthor = bookAuthor;
		this.releaseDate = releaseDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAutor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", bookTitle=" + bookTitle + ", bookAuthor=" + bookAuthor + ", releaseDate="
				+ releaseDate + "]";
	}
	
	
	
	
	

}
