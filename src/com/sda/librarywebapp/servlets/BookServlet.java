package com.sda.librarywebapp.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sda.librarywebapp.model.Book;
import com.sda.librarywebapp.service.BookService;

public class BookServlet extends HttpServlet {

	public void init() throws ServletException {
		System.out.println("Book servlet initialized");
	}

	public void destroy() {
		System.out.println("Book servlet is getting destroyed");
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("My GET request");
		response.setContentType("text/html");

		String idBook = request.getParameter("idBook");
		BookService.deleteBook(Integer.parseInt(idBook));

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("My GET request");
		response.setContentType("text/html");

		String idBook = request.getParameter("idBook");
		String bookTitle = request.getParameter("bookTitle");
		String bookAuthor = request.getParameter("bookAuthor");
		String releaseDate = request.getParameter("releaseDate");

		BookService.addBook(new Book(Integer.parseInt(idBook), bookTitle, bookAuthor, new Date()));
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("My GET request");
		response.setContentType("text/html");

		String idBook = request.getParameter("idBook");
		String bookTitle = request.getParameter("bookTitle");
		String bookAuthor = request.getParameter("bookAuthor");
		String releaseDate = request.getParameter("releaseDate");

		BookService.updateBook(new Book(Integer.parseInt(idBook), bookTitle, bookAuthor, new Date()));

	}

}
