package com.sda.librarywebapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import com.sda.librarywebapp.model.Book;

public class BookService {

	public static List<Book> bookList = new ArrayList<>();

	public static List<Book> listBooks() {

		if (bookList.isEmpty()) {
			Book book1 = new Book(1, "One Hundred Years of Solitude", "Gabriel Marquez", new Date());
			Book book2 = new Book(2, "We'll meet in August", "Gabriel Marquez", new Date());
			Book book3 = new Book(3, "Huliganii", "Mircea Eliade", new Date());
			Book book4 = new Book(4, "Amintiri din copilarie", "Ion Creanga", new Date());

			bookList.add(book1);
			bookList.add(book2);
			bookList.add(book3);
			bookList.add(book4);
		}

		return bookList;
	}

	public static void addBook(Book book) {
		bookList.add(book);
	}

	public static void deleteBook(int idBook) {
		// for(Book book: bookList) {
		// if(idBook==book.getId()) {
		// bookList.remove(book);
		// break;
		//
		Iterator<Book> it = bookList.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == idBook) {
				it.remove();
			}
		}
	}

	public static void updateBook(Book updateBook) {
		for (Book book : bookList) {
			if (book.getId() == updateBook.getId()) {
				book.setBookTitle(updateBook.getBookTitle());
				book.setBookAutor(updateBook.getBookAuthor());
				book.setReleaseDate(updateBook.getReleaseDate());
			}

		}

	}

	public static Book getBookById(int id) {
		for (Book book : bookList) {
			if (book.getId() == id) {
				return book;
			}
		
		}
		return null;
	}
}
