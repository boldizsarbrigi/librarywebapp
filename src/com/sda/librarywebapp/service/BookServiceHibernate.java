package com.sda.librarywebapp.service;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.sda.librarywebapp.model.Book;



public class BookServiceHibernate{
	
	public static List<Book> bookList = new ArrayList<>();
	private static SessionFactory sessionFactory;
	

	public static List<Book> listBooks() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		if (bookList.isEmpty()) {
		List<Book> books=session.createQuery("from Book").list();
		}
		return bookList;
	}

//	public static void addBook(Book book) {
//		bookList.add(book);
//	}
//
//	public static void deleteBook(int idBook) {
//		Iterator<Book> it = bookList.iterator();
//		while (it.hasNext()) {
//			if (it.next().getId() == idBook) {
//				it.remove();
//			}
//		}
//	}
//
//	public static void updateBook(Book updateBook) {
//		for (Book book : bookList) {
//			if (book.getId() == updateBook.getId()) {
//				book.setBookTitle(updateBook.getBookTitle());
//				book.setBookAutor(updateBook.getBookAuthor());
//				book.setReleaseDate(updateBook.getReleaseDate());
//			}
//
//		}
//
//	}
//
//	public static Book getBookById(int id) {
//		for (Book book : bookList) {
//			if (book.getId() == id) {
//				return book;
//			}
//		
//		}
//		return null;
//	}
//	

}
